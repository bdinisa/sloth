# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2022-03-30
### Added
- `sloth` CLI tool.
- README file.
- CHANGELOG file.

[0.1.0]: https://gitlab.mpi-sws.org/bdinisa/~/tags/v0.1.0

## [0.1.1] - 2022-04-27
### Added
- Support for microsecond times

[0.1.1]: https://gitlab.mpi-sws.org/bdinisa/~/tags/v0.1.1

## [0.2.0] - 2022-06-12
### Added
- Support for all durations and bandwidths (as supported by `tc`)
- Type-safe APIs for dealing with duration and bandwidths
- Conversion from `SlothConfig` back to JSON
- Extensive tests

[0.2.0]: https://gitlab.mpi-sws.org/bdinisa/~/tags/v0.2.0

## [0.3.0] - 2022-06-12
### Renamed
- `SlothConfig` -> `SlothNodeConfig`

### Changes
- CLI now expects a path to a high-level config
- CLI receives a hostname (can be autodetected)

### Added
- `SlothConfig` is now a high-level configuration of the whole topology
- A `SlothConfig` can compile `SlothNodeConfigs`

[0.3.0]: https://gitlab.mpi-sws.org/bdinisa/~/tags/v0.3.0

## [0.4.0] - 2022-06-16
### Renamed
- `interfaces` -> `nodes`

### Added
- `groups` field. Can now specify alias for groups of replicas. `@all` is an implicit group
- substitutions to make groups work


[0.4.0]: https://gitlab.mpi-sws.org/bdinisa/~/tags/v0.4.0

## [0.5.0] - 2022-06-20
### Changes
- Strict checks on ambiguous links

### Added
- `priority` field on links. Can be used to break ties


[0.5.0]: https://gitlab.mpi-sws.org/bdinisa/~/tags/v0.5.0
