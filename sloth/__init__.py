#!/usr/bin/env python3

##########################################################################################
# Copyright (c) Baltasar Dinis. All rights reserved.
# Licensed under the MIT License. See LICENSE in the project root for license information.
##########################################################################################

import logging
import click
import pkg_resources  # type: ignore

from .config import SlothConfig
import sloth.tcshim as tcshim
import socket

FORMAT = "%(asctime)s | %(name)s | %(levelname)s | %(message)s"
logging.basicConfig(format=FORMAT)
logger: logging.Logger = logging.getLogger("sloth")
logger.setLevel(logging.INFO)


@click.group()
@click.version_option(str(pkg_resources.get_distribution("sloth")), message="%(version)s")
def cli() -> int:
    """
    sloth: an easy-to-use, granular, traffic shaping tool based on Linux's tc
    """
    return 0


@click.command(help="print the configuration (after parsing)")
@click.argument("config", type=click.Path(exists=True))
@click.argument("hostname", required=False)
def echo(config: str, hostname: str) -> None:
    sloth_config = SlothConfig.from_file(click.format_filename(config))
    print(sloth_config)

    if hostname:
        print(sloth_config.compile(hostname))


@click.command(help="apply the network emulation, as per the configuration")
@click.argument("config", type=click.Path(exists=True))
@click.argument("hostname", default=socket.gethostname())
def apply(config: str, hostname: str) -> None:
    sloth_config = SlothConfig.from_file(click.format_filename(config)).compile(hostname)

    tcshim.show(sloth_config.interface)
    tcshim.clear(sloth_config.interface)
    tcshim.show(sloth_config.interface)
    tcshim.setup(sloth_config.interface)

    for idx, peer in enumerate(sloth_config.peers.values()):
        tcshim.add_latency_bw(sloth_config.interface, idx + 1, peer.ip, peer.lat_avg, peer.lat_stddev, peer.bandwidth)

    tcshim.show(sloth_config.interface)


@click.command(help="show the current network emulation, based on the configuration")
@click.argument("config", type=click.Path(exists=True))
@click.argument("hostname", default=socket.gethostname())
def show(config: str, hostname: str) -> None:
    sloth_config = SlothConfig.from_file(click.format_filename(config)).compile(hostname)
    tcshim.show(sloth_config.interface)


@click.command(help="clear the current network emulation, based on the configuration")
@click.argument("config", type=click.Path(exists=True))
@click.argument("hostname", default=socket.gethostname())
def clear(config: str, hostname: str) -> None:
    sloth_config = SlothConfig.from_file(click.format_filename(config)).compile(hostname)
    tcshim.clear(sloth_config.interface)


@click.command(help="show the current network emulation, given the network interface name")
@click.argument("iface")
def show_iface(iface: str) -> None:
    tcshim.show(iface)


@click.command(help="clear the current network emulation, given the network interface name")
@click.argument("iface")
def clear_iface(iface: str) -> None:
    tcshim.clear(iface)


cli.add_command(echo)
cli.add_command(apply)
cli.add_command(show)
cli.add_command(clear)
cli.add_command(show_iface)
cli.add_command(clear_iface)


def sloth_main() -> int:
    return cli(prog_name="sloth")
