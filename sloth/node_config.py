##########################################################################################
# Copyright (c) Baltasar Dinis. All rights reserved.
# Licensed under the MIT License. See LICENSE in the project root for license information.
##########################################################################################

"""
Sloth Node Config
"""

from typing import Any, Dict, Optional
import json
import socket

from .types import SlothDuration, SlothBandwidth


class SlothPeer:
    """
    model of a peer in the sloth context
    this includes what characteristics we wish to shape in the traffic
    """

    VALID_KEYS = {"hostname", "lat_avg", "lat_stddev", "bandwidth"}

    def __init__(self, hostname: str, lat_avg: Any = None, lat_stddev: Any = None, bandwidth: Any = None) -> None:
        if lat_stddev and lat_avg is None:
            raise ValueError("Cannot build sloth peer without lat avg but with lat stddev")

        self.hostname: str = hostname
        self.ip: str = socket.gethostbyname(self.hostname)
        self.lat_avg: Optional[SlothDuration] = SlothDuration.parse(lat_avg) if lat_avg else None
        self.lat_stddev: Optional[SlothDuration] = SlothDuration.parse(lat_stddev) if lat_stddev else None
        self.bandwidth: Optional[SlothBandwidth] = SlothBandwidth.parse(bandwidth) if bandwidth else None

    @classmethod
    def from_dict(cls, json_dict: Dict[Any, Any]) -> Any:
        """
        jarse a SlothPeer from the dict obtained from the JSON representation
        """
        if "hostname" not in json_dict:
            raise ValueError("peer without a 'hostname' key")

        if not isinstance(json_dict["hostname"], str):
            raise ValueError("'hostname' key must be a str")

        keys = set(json_dict.keys())
        if len(keys - SlothPeer.VALID_KEYS) != 0:
            raise ValueError(
                "unknown keys: {}".format(", ".join("'{}'".format(key) for key in list(keys - SlothPeer.VALID_KEYS)))
            )

        return cls(
            json_dict.get("hostname"), json_dict.get("lat_avg"), json_dict.get("lat_stddev"), json_dict.get("bandwidth")
        )

    def to_dict(self) -> Dict[str, Any]:
        """
        JSON compatible dict representation
        """

        d: Dict[Any, Any] = {"hostname": self.hostname}
        if self.lat_avg:
            d["lat_avg"] = repr(self.lat_avg)
        if self.lat_stddev:
            d["lat_stddev"] = repr(self.lat_stddev)
        if self.bandwidth:
            d["bandwidth"] = repr(self.bandwidth)

        return d

    def __repr__(self) -> str:
        return "SlothPeer(hostname={},ip={}{}{}{})".format(
            self.hostname,
            self.ip,
            ",lat_avg={}".format(self.lat_avg) if self.lat_avg else "",
            ",lat_stddev={}".format(self.lat_stddev) if self.lat_stddev else "",
            ",bandwidth={}".format(self.bandwidth) if self.bandwidth else "",
        )

    def __eq__(self, other: Any) -> bool:
        if other == None:
            return False
        if not isinstance(other, SlothPeer):
            raise TypeError("Incomparable types: SlothPeer <!> {}".format(type(other)))

        return (
            self.hostname == other.hostname
            and self.ip == other.ip
            and self.lat_avg == other.lat_avg
            and self.lat_stddev == other.lat_stddev
            and self.bandwidth == other.bandwidth
        )


class SlothNodeConfig:
    """
    The external representation of a sloth configuration
    """

    def __init__(self, interface: str) -> None:
        self.interface = interface
        self.peers: Dict[str, SlothPeer] = {}

    def add_peer(self, peer: SlothPeer) -> None:
        self.peers[peer.hostname] = peer

    @classmethod
    def from_dict(cls, json_dict: Dict[Any, Any]) -> Any:
        """
        build a sloth config from a dictionary obtained from its JSON representation
        """
        if "interface" not in json_dict:
            raise ValueError("config without a 'interface' key")

        if not isinstance(json_dict["interface"], str):
            raise ValueError("'interface' key must be a string")

        if "peers" not in json_dict:
            raise ValueError("config without a 'peers' key")

        if not isinstance(json_dict["peers"], list):
            raise ValueError("'peers' key must be a list")

        config = cls(json_dict["interface"])
        for peer in map(lambda x: SlothPeer.from_dict(x), json_dict["peers"]):
            config.add_peer(peer)

        return config

    def to_dict(self) -> Dict[str, Any]:
        """
        JSON compatible dict representation
        """

        return {"interface": self.interface, "peers": [p.to_dict() for p in self.peers.values()]}

    def __repr__(self) -> str:
        return "SlothNodeConfig(\n\tinterface={},\n\tpeers=\n{}\n)".format(
            self.interface, "\n".join("\t\t{}\t=> {}".format(k, v) for k, v in self.peers.items())
        )

    def __eq__(self, other: Any) -> bool:
        if other == None:
            return False
        if not isinstance(other, SlothNodeConfig):
            raise TypeError("Incomparable types: SlothNodeConfig <!> {}".format(type(other)))

        return self.interface == other.interface and self.peers == other.peers
