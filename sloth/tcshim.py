##########################################################################################
# Copyright (c) Baltasar Dinis. All rights reserved.
# Licensed under the MIT License. See LICENSE in the project root for license information.
##########################################################################################

"""
functions to interact with tc
"""

import subprocess
import shlex

import logging

logger: logging.Logger = logging.getLogger("sloth")
logger.setLevel(logging.INFO)

DEFAULT_BW: str = "1000000000gbit"


def show(iface: str) -> None:
    """show the TC queuing discipline on an interface"""
    output = subprocess.check_output(shlex.split(("tc qdisc show dev {}".format(iface)))).decode("utf-8")
    logger.info("{}: showing TC status".format(iface))
    for line in output.strip().split("\n"):
        print("{}: {}".format(iface, line))


def clear(iface: str) -> None:
    """delete all TC information on an interface"""
    res = execute("tc qdisc del dev {} root".format(iface))
    logger.info("{}: cleared interface".format(iface))


def create_root(iface: str) -> None:
    """delete the root where all other queuing disciplines are attached to"""
    res = execute("tc qdisc add dev {} root handle 1: htb".format(iface))
    res.check_returncode()
    logger.info("{}: created a root for interface".format(iface))


def setup(iface: str) -> None:
    """setup the network interface to add policies"""
    clear(iface)
    create_root(iface)


def create_class_bandwidth(iface: str, class_id: int, bw: str = None) -> None:
    """
    creates a bandwidth emulation class

    This is necessary *even if we do not wish to limit bandwidth*.
    This is due to an implementation detail in Linux TC:
        - bandwidth emulation is a *classful* queueing discipline
        - latency emulation is a *classless* queueing discipline

    As such, we need to create a class to emulate bandwidth,
    where we then attach:
        - a filter to limit the applicability of the emulations
        - a policy for latency emulation

    When we *do not* with to impose bandwidth limitations, we set the rate
    limiting to an absurdly large value, which in practice is never achieved
    by the physical medium.
    """

    bw = bw or DEFAULT_BW

    res = execute("tc class add dev {} parent 1: classid 1:{} htb rate {}".format(iface, class_id, bw))

    res.check_returncode()
    logger.info("{}: created class {} for interface at rate {}".format(iface, class_id, bw))


def create_filter(iface: str, class_id: int, ip: str) -> None:
    """
    creates a TC filter based on the destination IP address of a packet.

    The filter is attached to a TC class,
    being applied to all policies on the class
    """
    res = execute(
        "tc filter add dev {} parent 1: protocol ip prio 1 u32 flowid 1:{} match ip dst {}".format(iface, class_id, ip)
    )
    res.check_returncode()
    logger.info("{}: created a filter for ip {}".format(iface, ip))


def add_latency(iface: str, class_id: int, handle: int, avg: str, stddev: str = None) -> None:
    """
    add the latency emulation policy to a TC class

    if the standard deviation is specified, it follows a normal distribution
    """

    def add_fix_latency(iface: str, class_id: int, handle: int, avg: str) -> "subprocess.CompletedProcess[bytes]":
        return execute(
            "tc qdisc add dev {} parent 1:{} handle {}: netem delay {}".format(
                iface,
                class_id,
                handle,
                avg,
            )
        )

    def add_variable_latency(
        iface: str, class_id: int, handle: int, avg: str, stddev: str
    ) -> subprocess.CompletedProcess[bytes]:
        assert stddev is not None
        return execute(
            "tc qdisc add dev {} parent 1:{} handle {}: netem delay {} {} distribution normal".format(
                iface, class_id, handle, avg, stddev
            )
        )

    res = (
        add_fix_latency(iface, class_id, handle, avg)
        if stddev is None
        else add_variable_latency(iface, class_id, handle, avg, stddev)
    )
    res.check_returncode()
    logger.info("{}: added latency on class {}: {} {}".format(iface, class_id, avg, stddev))


def add_latency_bw(iface: str, class_id: int, ip: str, lat_avg: str, lat_stddev: str = None, bw: str = None) -> None:
    """
    helper to:
        - create a bandwidth emulation class in TC
        - create an destination IP based filter attached to that class
        - add a latency emulation policy to the class

    The class id must be unique
    """
    create_class_bandwidth(iface, class_id, bw)
    create_filter(iface, class_id, ip)
    add_latency(iface, class_id, 10 * class_id, lat_avg, lat_stddev)


def execute(cmd: str) -> "subprocess.CompletedProcess[bytes]":
    return subprocess.run(shlex.split(cmd), capture_output=True)
