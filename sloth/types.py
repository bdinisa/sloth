##########################################################################################
# Copyright (c) Baltasar Dinis. All rights reserved.
# Licensed under the MIT License. See LICENSE in the project root for license information.
##########################################################################################

from typing import Any, Dict, Optional
import re


class SlothDuration:
    UNITS = ["s", "ms", "us"]
    PATTERN = "^([0-9]+)(s|sec|secs|ms|msec|msecs|us|usec|usecs)?$"

    def __init__(self, value: int, unit: str = "ms") -> None:
        if unit not in SlothDuration.UNITS:
            ValueError(
                "Failed to create duration from ({}, {}): {} is not a valid unit [{}]".format(
                    value, unit, unit, SlothDuration.UNITS
                )
            )

        self.value = value
        self.unit = unit

    @classmethod
    def parse(cls, s: Any) -> Any:
        if isinstance(s, SlothDuration):
            return s
        if isinstance(s, int):
            return SlothDuration(s)
        elif not isinstance(s, str):
            raise TypeError("Cannot parse type {} into SlothBandwidth: {}".format(type(s), s))

        s = s.strip().replace(" ", "").lower()

        m = re.match(SlothDuration.PATTERN, s)

        if m is None:
            raise ValueError("failed to parse duration: {}", s)

        value = int(m.group(1))
        if m.group(2):
            unit = m.group(2).replace("secs", "s").replace("sec", "s")
            return cls(value, unit)
        else:
            return cls(value)

    def __repr__(self) -> str:
        return "{}{}".format(self.value, self.unit)

    def half(self) -> Any:
        """
        division by two
        """

        if self.unit == "us" or self.value % 2 == 0:
            return SlothDuration(self.value // 2, self.unit)
        else:
            return SlothDuration(self.value * 500, SlothDuration.UNITS[SlothDuration.UNITS.index(self.unit) + 1])

    def __eq__(self, other: Any) -> bool:
        if other == None:
            return False
        if not isinstance(other, SlothDuration):
            raise TypeError("Incomparable types: SlothDuration <!> {}".format(type(other)))

        self_unit_idx = SlothDuration.UNITS.index(self.unit)
        other_unit_idx = SlothDuration.UNITS.index(other.unit)

        if self_unit_idx < other_unit_idx:
            return self.value == other.value * (1000 ** (other_unit_idx - self_unit_idx))
        else:
            return other.value == self.value * (1000 ** (self_unit_idx - other_unit_idx))


class SlothBandwidth:
    UNITS = ["tbit", "gbit", "mbit", "kbit", "bit", "tibit", "gibit", "mibit", "kibit", "bit"]
    PATTERN = (
        "^([0-9]+)(tbps|gbps|mbps|kbps|bps|tbit|gbit|mbit|kbit|bit|tibps|gibps|mibps|kibps|tibit|gibit|mibit|kibit)?$"
    )

    def __init__(self, value: int, unit: str = "bit") -> None:
        if unit not in SlothBandwidth.UNITS:
            ValueError(
                "Failed to create bandwidth from ({}, {}): {} is not a valid unit [{}]".format(
                    value, unit, unit, SlothBandwidth.UNITS
                )
            )

        self.value = value
        self.unit = unit

    @classmethod
    def parse(cls, s: Any) -> Any:
        if isinstance(s, SlothBandwidth):
            return s
        if isinstance(s, int):
            return SlothBandwidth(s)
        elif not isinstance(s, str):
            raise TypeError("Cannot parse type {} into SlothBandwidth: {}".format(type(s), s))

        s = s.strip().replace(" ", "").lower()

        m = re.match(SlothBandwidth.PATTERN, s)

        if m is None:
            raise ValueError("failed to parse duration: {}", s)

        value = int(m.group(1))
        unit = m.group(2)
        if unit:
            if "bps" in unit:
                value *= 8
                unit = unit.replace("bps", "bit")

            return cls(value, unit)
        else:
            return cls(value)

    def __repr__(self) -> str:
        return "{}{}".format(self.value, self.unit)

    def half(self) -> Any:
        """
        division by two
        """

        if self.unit == "bit" or self.value % 2 == 0:
            return SlothBandwidth(self.value // 2, self.unit)
        elif "ibit" in self.unit:
            return SlothBandwidth(self.value * 512, SlothBandwidth.UNITS[SlothBandwidth.UNITS.index(self.unit) + 1])
        else:
            return SlothBandwidth(self.value * 500, SlothBandwidth.UNITS[SlothBandwidth.UNITS.index(self.unit) + 1])

    def to_bit(self) -> Any:
        unit_idx = SlothBandwidth.UNITS.index(self.unit)
        base_idx = SlothBandwidth.UNITS[unit_idx:].index("bit")
        base = 1024 if "ibit" in self.unit else 1000

        return SlothBandwidth(self.value * (base**base_idx), "bit")

    def __eq__(self, other: Any) -> bool:
        if other == None:
            return False
        if not isinstance(other, SlothBandwidth):
            raise TypeError("Incomparable types: SlothBandwidth <!> {}".format(type(other)))
        return self.to_bit().value == other.to_bit().value
