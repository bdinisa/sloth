##########################################################################################
# Copyright (c) Baltasar Dinis. All rights reserved.
# Licensed under the MIT License. See LICENSE in the project root for license information.
##########################################################################################

"""
Sloth Config
"""

from typing import Any, Dict, Tuple, Optional, List, Set
from collections import defaultdict
import bisect
import json
from .types import SlothDuration, SlothBandwidth
from .node_config import SlothNodeConfig, SlothPeer


class SlothLink:
    VALID_KEYS = {"priority", "hosts", "lat_avg", "lat_stddev", "bandwidth", "directional"}

    def __init__(
        self,
        src: str,
        dst: str,
        priority: Optional[int] = None,
        lat_avg: Any = None,
        lat_stddev: Any = None,
        bandwidth: Any = None,
        directional: bool = False,
    ) -> None:
        if lat_stddev and lat_avg is None:
            raise ValueError("Cannot build sloth peer without lat avg but with lat stddev")

        self.src: str = src
        self.dst: str = dst
        self.priority: Optional[int] = priority
        self.lat_avg: Optional[SlothDuration] = SlothDuration.parse(lat_avg) if lat_avg else None
        self.lat_stddev: Optional[SlothDuration] = SlothDuration.parse(lat_stddev) if lat_stddev else None
        self.bandwidth: Optional[SlothBandwidth] = SlothBandwidth.parse(bandwidth) if bandwidth else None
        self.directional: bool = directional

    @classmethod
    def from_dict(cls, json_dict: Dict[Any, Any]) -> Any:
        """
        build a sloth link from a dictionary obtained from its JSON representation
        """

        if "hosts" not in json_dict:
            raise ValueError("link without a 'hosts' key")

        if not isinstance(json_dict["hosts"], list):
            raise ValueError("'hosts' key must be a list")

        if len(json_dict["hosts"]) != 2:
            raise ValueError("'hosts' must have two elements")

        if not all(isinstance(host, str) for host in json_dict["hosts"]):
            raise ValueError("'hosts' values must be strings")

        keys = set(json_dict.keys())
        if len(keys - SlothLink.VALID_KEYS) != 0:
            raise ValueError(
                "unknown keys: {}".format(", ".join("'{}'".format(key) for key in list(keys - SlothLink.VALID_KEYS)))
            )

        src = json_dict["hosts"][0]
        dst = json_dict["hosts"][1]

        priority = None
        if "priority" in json_dict:
            priority = int(json_dict["priority"])

        return cls(
            src,
            dst,
            priority,
            json_dict.get("lat_avg"),
            json_dict.get("lat_stddev"),
            json_dict.get("bandwidth"),
            json_dict.get("directional") or False,
        )

    def instantiate(self, src: str, dst: str) -> Any:
        """
        generate a concrete sloth link from a generic (ie: with group endpoints)
        """
        return SlothLink(src, dst, self.priority, self.lat_avg, self.lat_stddev, self.bandwidth, self.directional)

    def generate_peer(self, hostname: str) -> Optional[SlothPeer]:
        if self.directional:
            if self.src != hostname:
                return None

            return SlothPeer(self.dst, self.lat_avg, self.lat_stddev, self.bandwidth)
        elif self.src == hostname:
            return SlothPeer(
                self.dst,
                self.lat_avg.half() if self.lat_avg else None,
                self.lat_stddev.half() if self.lat_stddev else None,
                self.bandwidth,
            )
        elif self.dst == hostname:
            return SlothPeer(
                self.src,
                self.lat_avg.half() if self.lat_avg else None,
                self.lat_stddev.half() if self.lat_stddev else None,
                self.bandwidth,
            )

        return None

    def __repr__(self) -> str:
        return "SlothLink(direction={},src={},dst={}{}{}{}{})".format(
            "->" if self.directional else "<->",
            self.src,
            self.dst,
            ",priority={}".format(self.priority) if self.priority else "",
            ",lat_avg={}".format(self.lat_avg) if self.lat_avg else "",
            ",lat_stddev={}".format(self.lat_stddev) if self.lat_stddev else "",
            ",bandwidth={}".format(self.bandwidth) if self.bandwidth else "",
        )

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, SlothLink):
            return False

        if self.directional != other.directional:
            return False

        if self.directional:
            return (
                set([self.src, self.dst]) == set([other.src, other.dst])
                and self.priority == other.priority
                and self.lat_avg == other.lat_avg
                and self.lat_stddev == other.lat_stddev
                and self.bandwidth == other.bandwidth
            )
        else:
            return (
                self.src == other.src
                and self.dst == other.dst
                and self.priority == other.priority
                and self.lat_avg == other.lat_avg
                and self.lat_stddev == other.lat_stddev
                and self.bandwidth == other.bandwidth
            )


class SlothConfig:
    """
    The external representation of a sloth configuration

    There are three main elements of the config:
        - the nodes: gives information about which nodes there are in the system, and what interfaces they communicate through
        - the links: description of the characteristics of each link
        - the groups: shorthand alias for groups of nodes. If node X has the same type of link to nodes Y and Z, Y and Z can be called `xpeers' and we can describe their link in a single place
    """

    VALID_KEYS = {"nodes", "links", "groups"}

    def __init__(self, nodes: Dict[str, str], groups: Dict[str, List[str]]) -> None:
        self.nodes: Dict[str, str] = nodes
        self.configs: Dict[str, SlothNodeConfig] = {}
        self.links: List[SlothLink] = []
        self.groups: Dict[str, List[str]] = {"@all": list(self.nodes.keys())}

        for name, group in groups.items():
            if not SlothConfig._is_group(name):
                raise ValueError("invalid group name: {}", name)

            if name == "@all":
                raise ValueError("invalid group name: @all is a reserved name")

            unknown_nodes = set(group) - set(list(self.nodes.keys()))
            if len(unknown_nodes) > 0:
                raise ValueError("invalid group {}: unknown nodes: {}", name, ", ".join(unknown_nodes))

            self.groups[name] = group

    def add_link(self, link: SlothLink) -> None:
        if link.src not in self.nodes and link.src not in self.groups:
            raise ValueError("unknown node: {} (link: {})".format(link.src, link))
        if link.dst not in self.nodes and link.dst not in self.groups:
            raise ValueError("unknown node: {} (link: {})".format(link.dst, link))
        self.links.append(link)

    @classmethod
    def from_file(cls, filename: str) -> Any:
        """
        read a sloth config from a file
        """
        json_dict = json.load(open(filename, "r"))

        return cls.from_dict(json_dict)

    @classmethod
    def from_dict(cls, json_dict: Dict[Any, Any]) -> Any:
        """
        build a sloth config from a dictionary obtained from its JSON representation
        """

        if "nodes" not in json_dict:
            raise ValueError("config without a 'nodes' key")

        if not isinstance(json_dict["nodes"], dict):
            raise ValueError("'nodes' key must be a dict")

        if not all(isinstance(key, str) for key in json_dict["nodes"].keys()):
            raise ValueError("keys of the 'nodes' dict must be strings")

        if not all(isinstance(value, str) for value in json_dict["nodes"].values()):
            raise ValueError("values of the 'nodes' dict must be strings")

        if "groups" in json_dict:
            if not isinstance(json_dict["groups"], dict):
                raise ValueError("'groups' key must be a list")

            if not all(isinstance(key, str) for key in json_dict["groups"].keys()):
                raise ValueError("keys of the 'groups' dict must be strings")

            if not all(
                isinstance(value, list) and all(isinstance(node, str) for node in value)
                for value in json_dict["groups"].values()
            ):
                raise ValueError("values of the 'groups' dict must be lists of strings")

        if "links" not in json_dict:
            raise ValueError("config without a 'links' key")

        if not isinstance(json_dict["links"], list):
            raise ValueError("'links' key must be a list")

        keys = set(json_dict.keys())
        if len(keys - SlothConfig.VALID_KEYS) != 0:
            raise ValueError(
                "unknown keys: {}".format(", ".join("'{}'".format(key) for key in list(keys - SlothConfig.VALID_KEYS)))
            )

        config = cls(json_dict["nodes"], json_dict.get("groups") or {})

        for link in map(lambda x: SlothLink.from_dict(x), json_dict["links"]):
            config.add_link(link)

        return config

    @staticmethod
    def _is_group(name: str) -> bool:
        return name.startswith("@")

    def _in_group(self, host: str, group: str) -> bool:
        if not SlothConfig._is_group(group):
            # this is not a group
            return False

        return host in self.groups[group]

    def _links_for_host(self, host: str) -> List[SlothLink]:
        """
        generator for links that are relevant for a particular host,
        making them concrete (ie: without groups)

        Filter if host is in neither.

        Otherwise, expansion works as follows
        There are 4 cases:
            A. [host-host]. Nothing is done.
            B. [host-group], with host == target host.  Expand group with group - {host}.
            C. [group-host], with target host in group. Replace group with host.
            D. [group-group]. Replace group by host (possibly in two distinct copies if the host is in both). Repeat B

        There needs to be a priority scheme.
        In case the expansion yields multiple links with the same hosts, there
        needs to be some resolution to choose the appropriate link.

        The resolution strategy comes from the order of the cases (A through D).

        Since B and C are simmetric (ie: a B link for the host is
        a C link for the destination and vice versa), they are on the same level.

        If there are multiple links at the same level,
        this represents a bug and an exception is raised
        """

        def _generate_key(src: str, dst: str, directional: bool) -> Tuple[str, str]:
            if directional:
                return (src, dst)
            key_l: List[str] = sorted((src, dst))
            key: Tuple[str, str] = (key_l[0], key_l[1])
            return key

        level_priority: Dict[Tuple[str, str], List[SlothLink]] = defaultdict(lambda: [])
        level_a: Dict[Tuple[str, str], List[SlothLink]] = defaultdict(lambda: [])
        level_bc: Dict[Tuple[str, str], List[SlothLink]] = defaultdict(lambda: [])
        level_d: Dict[Tuple[str, str], List[SlothLink]] = defaultdict(lambda: [])

        def _add_to(level: str, src: str, dst: str, link: SlothLink) -> None:
            key = _generate_key(src, dst, link.directional)
            if link.priority is not None:
                if link not in level_priority[key]:
                    bisect.insort(level_priority[key], link, key=lambda x: x.priority or 0)
            elif level == "A":
                if link not in level_a[key]:
                    level_a[key].append(link)
            elif level == "B":
                if link not in level_bc[key]:
                    level_bc[key].append(link)
            elif level == "C":
                if link not in level_bc[key]:
                    level_bc[key].append(link)
            elif level == "D":
                if link not in level_d[key]:
                    level_d[key].append(link)
            else:
                raise ValueError("unknown level {}".format(level))

        for link in filter(
            lambda l: host in (l.src, l.dst) or self._in_group(host, l.src) or self._in_group(host, l.dst), self.links
        ):
            # A
            if not SlothConfig._is_group(link.src) and not SlothConfig._is_group(link.dst):
                _add_to("A", link.src, link.dst, link)

            if not SlothConfig._is_group(link.src) and SlothConfig._is_group(link.dst):
                # B
                if link.src == host:
                    for peer in filter(lambda h: h != host, self.groups[link.dst]):
                        _add_to("B", host, peer, link)

                # C
                else:  # self._in_group(host, link.dst)
                    _add_to("C", link.src, host, link)

            if SlothConfig._is_group(link.src) and not SlothConfig._is_group(link.dst):
                # C
                if self._in_group(host, link.src) and link.dst != host:
                    _add_to("C", host, link.dst, link)

                # B
                else:  # host == link.dst
                    for peer in filter(lambda h: h != host, self.groups[link.src]):
                        _add_to("B", peer, host, link)

            # D
            if SlothConfig._is_group(link.src) and SlothConfig._is_group(link.dst):
                if self._in_group(host, link.src):
                    for peer in filter(lambda h: h != host, self.groups[link.dst]):
                        _add_to("D", host, peer, link)

                if self._in_group(host, link.dst):
                    for peer in filter(lambda h: h != host, self.groups[link.src]):
                        _add_to("D", peer, host, link)

        keys: Set[Tuple[str, str]] = set(
            sum(map(lambda x: list(x.keys()), [level_a, level_bc, level_d, level_priority]), [])
        )

        links: List[SlothLink] = []

        for key in keys:
            if key in level_priority:
                max_priority: int = level_priority[key][-1].priority or 0
                max_priority_links: List[SlothLink] = list(
                    filter(lambda l: l.priority == max_priority, level_priority[key])
                )
                if len(max_priority_links) > 1:
                    raise ValueError("ambiguous links for {} at priority {}: {}", key, max_priority, max_priority_links)
                links.append(max_priority_links[0].instantiate(key[0], key[1]))
            elif key in level_a:
                if len(level_a[key]) > 1:
                    raise ValueError("ambiguous links for {}: {}", key, level_a[key])
                links.append(level_a[key][0].instantiate(key[0], key[1]))
            elif key in level_bc:
                if len(level_bc[key]) > 1:
                    raise ValueError("ambiguous links for {}: {}", key, level_bc[key])
                links.append(level_bc[key][0].instantiate(key[0], key[1]))
            elif key in level_d:
                if len(level_d[key]) > 1:
                    raise ValueError("ambiguous links for {}: {}", key, level_d[key])
                links.append(level_d[key][0].instantiate(key[0], key[1]))

        return links

    def compile(self, host: str) -> SlothNodeConfig:
        """
        compile this configuration for a host
        """

        if host in self.configs:
            return self.configs[host]

        if host not in self.nodes:
            raise ValueError("Unknown host: {}".format(host))

        config = SlothNodeConfig(self.nodes[host])
        for link in self._links_for_host(host):
            peer = link.generate_peer(host)
            if peer is not None:
                config.add_peer(peer)

        self.configs[host] = config
        return config

    def __repr__(self) -> str:
        return "SlothConfig(\n\tnodes={},\n\tlinks=\n{}\n)".format(
            ", ".join("{}:{}".format(k, v) for k, v in self.nodes.items()),
            "\n".join("\t\t{}".format(v) for v in self.links),
        )
