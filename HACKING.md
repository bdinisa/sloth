# Sloth Developer Notes

This documents contains useful information for developers that want to help in the development of Sloth.

# Dependencies

Check [setup.cfg](setup.cfg) file for the list of dependencies.

# Contributing Guidelines

 1. Use type annotations: it is easier to read the code in local context
 1. Any python tests should be placed in the `tests` directory.
 1. Always run `tox` before submitting your contributions.


# Package Structure

|File | Description|
|-----|----------|
|`tcshim.py` | Expose a high-level shim library to execute Linux `tc` commands |
|`types.py` | Type-safe model for duration and bandwidth, fully compatible with `tc`'s accepted formats |
|`node_config.py` | Model for the config being applied at a particular node |
|`config.py` | Model for the high-level topology config |
