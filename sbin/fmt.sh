#!/usr/bin/env sh

.tox/format/bin/black --line-length=120 sloth
.tox/format/bin/black --line-length=120 tests
