#!/usr/bin/env sh

.tox/typecheck/bin/mypy --strict --no-strict-optional --no-warn-return-any --allow-subclassing-any --namespace-packages sloth tests
