##########################################################################################
# Copyright (c) Baltasar Dinis. All rights reserved.
# Licensed under the MIT License. See LICENSE in the project root for license information.
##########################################################################################

"""
config_test.py

config.py unit tests
"""

import unittest
import unittest.mock as mock

from sloth.config import SlothConfig
from sloth.node_config import SlothNodeConfig

FAKE_DNS = {
    "machine01": "192.168.0.1",
    "machine02": "192.168.0.2",
    "machine03": "192.168.0.3",
    "machine04": "192.168.0.4",
    "machine05": "192.168.0.5",
}


class TestSlothConfig(unittest.TestCase):
    def test_failed_parsing(self) -> None:
        with self.assertRaises(ValueError):
            # missing links
            SlothConfig.from_dict({"nodes": {}, "groups": {}})

        with self.assertRaises(ValueError):
            # links incorrect type
            SlothConfig.from_dict({"nodes": {}, "groups": {}, "links": {}})

        with self.assertRaises(ValueError):
            # missing nodes
            SlothConfig.from_dict({"groups": {}, "links": []})

        with self.assertRaises(ValueError):
            # nodes incorrect type
            SlothConfig.from_dict({"nodes": [], "groups": {}, "links": []})

        with self.assertRaises(ValueError):
            # nodes incorrect type
            SlothConfig.from_dict({"nodes": {"a": 1}, "groups": {}, "links": []})

        with self.assertRaises(ValueError):
            # nodes incorrect type
            SlothConfig.from_dict({"nodes": {1: "a"}, "groups": {}, "links": []})

        with self.assertRaises(ValueError):
            # groups incorrect type
            SlothConfig.from_dict({"nodes": {}, "groups": [], "links": []})

        with self.assertRaises(ValueError):
            # groups incorrect type
            SlothConfig.from_dict({"nodes": {}, "groups": {1: []}, "links": []})

        with self.assertRaises(ValueError):
            # groups incorrect type
            SlothConfig.from_dict({"nodes": {}, "groups": {1: "str"}, "links": []})

        with self.assertRaises(ValueError):
            # groups incorrect type
            SlothConfig.from_dict({"nodes": {}, "groups": {"@x": "str"}, "links": []})

        with self.assertRaises(ValueError):
            # groups invalid value
            SlothConfig.from_dict({"nodes": {}, "groups": {"@all": []}, "links": []})

        with self.assertRaises(ValueError):
            # extra key
            SlothConfig.from_dict({"nodes": {}, "groups": {}, "links": [], "extra": 1})

        with self.assertRaises(ValueError):
            # unknown node in link
            SlothConfig.from_dict(
                {"nodes": {"machine01": "eth0"}, "groups": {}, "links": [{"hosts": ["machine01", "machine02"]}]}
            )

        with self.assertRaises(ValueError):
            # unknown node in link
            SlothConfig.from_dict(
                {"nodes": {"machine01": "eth0"}, "groups": {}, "links": [{"hosts": ["machine02", "machine01"]}]}
            )

        with self.assertRaises(ValueError):
            # unknown node in group
            SlothConfig.from_dict({"nodes": {"machine01": "eth0"}, "groups": {"@x": ["machine02"]}, "links": []})

        # ok
        SlothConfig.from_dict(
            {"nodes": {"machine01": "eth0", "machine02": "eth0"}, "links": [{"hosts": ["machine01", "machine02"]}]}
        )
        SlothConfig.from_dict(
            {
                "nodes": {"machine01": "eth0", "machine02": "eth0"},
                "groups": {"@x": ["machine01"]},
                "links": [{"hosts": ["machine01", "machine02"]}, {"hosts": ["@all", "@x"]}],
            }
        )

    @mock.patch("sloth.node_config.socket.gethostbyname")
    def test_config(self, gethostbyname_mock: mock.Mock) -> None:
        """
        test if a config is compiling to the correct configs
        """
        gethostbyname_mock.side_effect = lambda hostname: FAKE_DNS[hostname]

        json_dict = {
            "nodes": {"machine01": "eth0", "machine02": "wpl3s0", "machine03": "bond0"},
            "links": [
                {"hosts": ["machine01", "machine02"], "lat_avg": "20ms", "lat_stddev": "5ms"},
                {"hosts": ["machine02", "machine03"], "lat_avg": "10ms", "lat_stddev": "5ms", "bandwidth": "5Mbit"},
                {"hosts": ["machine01", "machine03"], "lat_avg": "100ms", "lat_stddev": "5ms", "directional": True},
                {"hosts": ["machine03", "machine01"], "lat_avg": "1ms", "lat_stddev": "1ms", "directional": True},
            ],
        }

        config = SlothConfig.from_dict(json_dict)
        self.assertEqual(
            config.compile("machine01"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine02", "lat_avg": "10ms", "lat_stddev": "2500us"},
                        {"hostname": "machine03", "lat_avg": "100ms", "lat_stddev": "5ms"},
                    ],
                }
            ),
        )
        self.assertEqual(
            config.compile("machine02"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "wpl3s0",
                    "peers": [
                        {"hostname": "machine01", "lat_avg": "10ms", "lat_stddev": "2500us"},
                        {"hostname": "machine03", "lat_avg": "5ms", "lat_stddev": "2500us", "bandwidth": "5Mbit"},
                    ],
                }
            ),
        )
        self.assertEqual(
            config.compile("machine03"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "bond0",
                    "peers": [
                        {"hostname": "machine01", "lat_avg": "1ms", "lat_stddev": "1ms"},
                        {"hostname": "machine02", "lat_avg": "5ms", "lat_stddev": "2500us", "bandwidth": "5Mbit"},
                    ],
                }
            ),
        )

        with self.assertRaises(ValueError):
            config.compile("machine04")

    @mock.patch("sloth.node_config.socket.gethostbyname")
    def test_all_scaffolding(self, gethostbyname_mock: mock.Mock) -> None:
        """
        test if a config is doing all scaffolding correctly
        """
        gethostbyname_mock.side_effect = lambda hostname: FAKE_DNS[hostname]

        json_dict = {
            "nodes": {
                "machine01": "eth0",
                "machine02": "eth0",
                "machine03": "eth0",
                "machine04": "eth0",
                "machine05": "eth0",
            },
            "links": [
                {"hosts": ["machine01", "@all"], "lat_avg": "2ms"},
                {"hosts": ["@all", "@all"], "lat_avg": "6ms"},
                {"hosts": ["machine05", "machine01"], "lat_avg": "10ms"},
                {"hosts": ["machine05", "machine02"], "lat_avg": "20ms"},
                {"hosts": ["machine05", "machine03"], "lat_avg": "30ms"},
                {"hosts": ["machine05", "machine04"], "lat_avg": "40ms"},
            ],
        }

        config = SlothConfig.from_dict(json_dict)
        self.assertEqual(
            config.compile("machine01"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine02", "lat_avg": "1ms"},
                        {"hostname": "machine03", "lat_avg": "1ms"},
                        {"hostname": "machine04", "lat_avg": "1ms"},
                        {"hostname": "machine05", "lat_avg": "5ms"},
                    ],
                }
            ),
        )

        self.assertEqual(
            config.compile("machine02"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine01", "lat_avg": "1ms"},
                        {"hostname": "machine03", "lat_avg": "3ms"},
                        {"hostname": "machine04", "lat_avg": "3ms"},
                        {"hostname": "machine05", "lat_avg": "10ms"},
                    ],
                }
            ),
        )

        self.assertEqual(
            config.compile("machine03"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine01", "lat_avg": "1ms"},
                        {"hostname": "machine02", "lat_avg": "3ms"},
                        {"hostname": "machine04", "lat_avg": "3ms"},
                        {"hostname": "machine05", "lat_avg": "15ms"},
                    ],
                }
            ),
        )

        self.assertEqual(
            config.compile("machine04"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine01", "lat_avg": "1ms"},
                        {"hostname": "machine02", "lat_avg": "3ms"},
                        {"hostname": "machine03", "lat_avg": "3ms"},
                        {"hostname": "machine05", "lat_avg": "20ms"},
                    ],
                }
            ),
        )

        self.assertEqual(
            config.compile("machine05"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine01", "lat_avg": "5ms"},
                        {"hostname": "machine02", "lat_avg": "10ms"},
                        {"hostname": "machine03", "lat_avg": "15ms"},
                        {"hostname": "machine04", "lat_avg": "20ms"},
                    ],
                }
            ),
        )

    @mock.patch("sloth.node_config.socket.gethostbyname")
    def test_group_scaffolding(self, gethostbyname_mock: mock.Mock) -> None:
        """
        test if a config is doing all scaffolding correctly
        """
        gethostbyname_mock.side_effect = lambda hostname: FAKE_DNS[hostname]

        json_dict = {
            "nodes": {
                "machine01": "eth0",
                "machine02": "eth0",
                "machine03": "eth0",
                "machine04": "eth0",
                "machine05": "eth0",
            },
            "groups": {
                "@a": ["machine01", "machine02"],
                "@b": ["machine03", "machine04"],
            },
            "links": [
                {"hosts": ["@a", "@b"], "lat_avg": "4ms"},
                {"hosts": ["machine01", "@b"], "lat_avg": "6ms"},
                {"hosts": ["machine05", "@all"], "lat_avg": "8ms"},
                {"hosts": ["machine05", "machine01"], "lat_avg": "10ms"},
                {"hosts": ["machine05", "machine02"], "lat_avg": "20ms"},
                {"hosts": ["machine05", "machine03"], "lat_avg": "30ms"},
                {"hosts": ["machine05", "machine04"], "lat_avg": "40ms"},
            ],
        }

        config = SlothConfig.from_dict(json_dict)
        self.assertEqual(
            config.compile("machine01"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine03", "lat_avg": "3ms"},
                        {"hostname": "machine04", "lat_avg": "3ms"},
                        {"hostname": "machine05", "lat_avg": "5ms"},
                    ],
                }
            ),
        )

        self.assertEqual(
            config.compile("machine02"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine03", "lat_avg": "2ms"},
                        {"hostname": "machine04", "lat_avg": "2ms"},
                        {"hostname": "machine05", "lat_avg": "10ms"},
                    ],
                }
            ),
        )

        self.assertEqual(
            config.compile("machine03"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine01", "lat_avg": "3ms"},
                        {"hostname": "machine02", "lat_avg": "2ms"},
                        {"hostname": "machine05", "lat_avg": "15ms"},
                    ],
                }
            ),
        )

        self.assertEqual(
            config.compile("machine04"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine01", "lat_avg": "3ms"},
                        {"hostname": "machine02", "lat_avg": "2ms"},
                        {"hostname": "machine05", "lat_avg": "20ms"},
                    ],
                }
            ),
        )

        self.assertEqual(
            config.compile("machine05"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine01", "lat_avg": "5ms"},
                        {"hostname": "machine02", "lat_avg": "10ms"},
                        {"hostname": "machine03", "lat_avg": "15ms"},
                        {"hostname": "machine04", "lat_avg": "20ms"},
                    ],
                }
            ),
        )

    @mock.patch("sloth.node_config.socket.gethostbyname")
    def test_conflicts_a(self, gethostbyname_mock: mock.Mock) -> None:
        """
        test that conflicts are being correctly raised at level A
        """
        gethostbyname_mock.side_effect = lambda hostname: FAKE_DNS[hostname]

        json_dict = {
            "nodes": {
                "machine01": "eth0",
                "machine02": "eth0",
            },
            "links": [
                {"hosts": ["machine01", "machine02"], "lat_avg": "6ms"},
                {"hosts": ["machine01", "machine02"], "lat_avg": "3ms"},
            ],
        }

        config = SlothConfig.from_dict(json_dict)
        with self.assertRaises(ValueError):
            config.compile("machine01")

        with self.assertRaises(ValueError):
            config.compile("machine02")

    @mock.patch("sloth.node_config.socket.gethostbyname")
    def test_conflicts_bc(self, gethostbyname_mock: mock.Mock) -> None:
        """
        test that conflicts are being correctly raised at levels B and C
        """
        gethostbyname_mock.side_effect = lambda hostname: FAKE_DNS[hostname]

        json_dict = {
            "nodes": {
                "machine01": "eth0",
                "machine02": "eth0",
            },
            "links": [
                {"hosts": ["@all", "machine01"], "lat_avg": "6ms"},
                {"hosts": ["@all", "machine02"], "lat_avg": "3ms"},
            ],
        }

        config = SlothConfig.from_dict(json_dict)
        with self.assertRaises(ValueError):
            config.compile("machine01")

        with self.assertRaises(ValueError):
            config.compile("machine02")

    @mock.patch("sloth.node_config.socket.gethostbyname")
    def test_conflicts_d(self, gethostbyname_mock: mock.Mock) -> None:
        """
        test that conflicts are being correctly raised at level B
        """
        gethostbyname_mock.side_effect = lambda hostname: FAKE_DNS[hostname]

        json_dict = {
            "nodes": {
                "machine01": "eth0",
                "machine02": "eth0",
            },
            "groups": {
                "@a": ["machine01"],
                "@b": ["machine02"],
            },
            "links": [
                {"hosts": ["@all", "@a"], "lat_avg": "6ms"},
                {"hosts": ["@all", "@b"], "lat_avg": "3ms"},
            ],
        }

        config = SlothConfig.from_dict(json_dict)
        with self.assertRaises(ValueError):
            config.compile("machine01")

        with self.assertRaises(ValueError):
            config.compile("machine02")

    @mock.patch("sloth.node_config.socket.gethostbyname")
    def test_conflicts_priority(self, gethostbyname_mock: mock.Mock) -> None:
        """
        test that conflicts are being correctly raised at level B
        """
        gethostbyname_mock.side_effect = lambda hostname: FAKE_DNS[hostname]

        json_dict = {
            "nodes": {
                "machine01": "eth0",
                "machine02": "eth0",
            },
            "groups": {
                "@a": ["machine01"],
                "@b": ["machine02"],
            },
            "links": [
                {"hosts": ["@all", "@a"], "lat_avg": "6ms", "priority": 1},
                {"hosts": ["@all", "@b"], "lat_avg": "3ms", "priority": 1},
            ],
        }

        config = SlothConfig.from_dict(json_dict)
        with self.assertRaises(ValueError):
            config.compile("machine01")

        with self.assertRaises(ValueError):
            config.compile("machine02")

    @mock.patch("sloth.node_config.socket.gethostbyname")
    def test_priority(self, gethostbyname_mock: mock.Mock) -> None:
        """
        test that conflicts are being correctly raised at level B
        """
        gethostbyname_mock.side_effect = lambda hostname: FAKE_DNS[hostname]

        json_dict = {
            "nodes": {
                "machine01": "eth0",
                "machine02": "eth0",
                "machine03": "eth0",
                "machine04": "eth0",
            },
            "groups": {
                "@a": ["machine01", "machine02"],
            },
            "links": [
                {"hosts": ["@a", "@a"], "lat_avg": "2ms", "priority": 3},
                {"hosts": ["@a", "@all"], "lat_avg": "4ms", "priority": 2},
                {"hosts": ["@all", "@all"], "lat_avg": "6ms", "priority": 1},
            ],
        }

        config = SlothConfig.from_dict(json_dict)
        self.assertEqual(
            config.compile("machine01"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine02", "lat_avg": "1ms"},
                        {"hostname": "machine03", "lat_avg": "2ms"},
                        {"hostname": "machine04", "lat_avg": "2ms"},
                    ],
                }
            ),
        )

        self.assertEqual(
            config.compile("machine02"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine01", "lat_avg": "1ms"},
                        {"hostname": "machine03", "lat_avg": "2ms"},
                        {"hostname": "machine04", "lat_avg": "2ms"},
                    ],
                }
            ),
        )

        self.assertEqual(
            config.compile("machine03"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine01", "lat_avg": "2ms"},
                        {"hostname": "machine02", "lat_avg": "2ms"},
                        {"hostname": "machine04", "lat_avg": "3ms"},
                    ],
                }
            ),
        )

        self.assertEqual(
            config.compile("machine04"),
            SlothNodeConfig.from_dict(
                {
                    "interface": "eth0",
                    "peers": [
                        {"hostname": "machine01", "lat_avg": "2ms"},
                        {"hostname": "machine02", "lat_avg": "2ms"},
                        {"hostname": "machine03", "lat_avg": "3ms"},
                    ],
                }
            ),
        )
