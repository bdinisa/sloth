##########################################################################################
# Copyright (c) Baltasar Dinis. All rights reserved.
# Licensed under the MIT License. See LICENSE in the project root for license information.
##########################################################################################

"""
node_config_test.py

node_config.py unit tests
"""

import unittest
import unittest.mock as mock

from sloth.node_config import SlothNodeConfig, SlothPeer
from sloth.types import SlothDuration, SlothBandwidth

FAKE_DNS = {
    "machine01": "192.168.0.1",
    "machine02": "192.168.0.2",
    "machine03": "192.168.0.3",
    "machine04": "192.168.0.4",
}


class TestSlothPeer(unittest.TestCase):
    @mock.patch("sloth.node_config.socket.gethostbyname")
    def test_init(self, gethostbyname_mock: mock.Mock) -> None:
        gethostbyname_mock.side_effect = lambda hostname: FAKE_DNS[hostname]

        peer = SlothPeer("machine01")
        self.assertEqual(peer.hostname, "machine01")
        self.assertEqual(peer.ip, "192.168.0.1")
        self.assertEqual(peer.lat_avg, None)
        self.assertEqual(peer.lat_stddev, None)
        self.assertEqual(peer.bandwidth, None)

        peer = SlothPeer("machine01", "42ms")
        self.assertEqual(peer.hostname, "machine01")
        self.assertEqual(peer.ip, "192.168.0.1")
        self.assertEqual(peer.lat_avg, SlothDuration(42, "ms"))
        self.assertEqual(peer.lat_stddev, None)
        self.assertEqual(peer.bandwidth, None)

        peer = SlothPeer("machine01", "42ms", None, "21kbit")
        self.assertEqual(peer.hostname, "machine01")
        self.assertEqual(peer.ip, "192.168.0.1")
        self.assertEqual(peer.lat_avg, SlothDuration(42, "ms"))
        self.assertEqual(peer.lat_stddev, None)
        self.assertEqual(peer.bandwidth, SlothBandwidth(21, "kbit"))

        peer = SlothPeer("machine01", None, None, "21kbit")
        self.assertEqual(peer.hostname, "machine01")
        self.assertEqual(peer.ip, "192.168.0.1")
        self.assertEqual(peer.lat_avg, None)
        self.assertEqual(peer.lat_stddev, None)
        self.assertEqual(peer.bandwidth, SlothBandwidth(21, "kbit"))

        peer = SlothPeer("machine01", "42ms", "400us", "21kbit")
        self.assertEqual(peer.hostname, "machine01")
        self.assertEqual(peer.ip, "192.168.0.1")
        self.assertEqual(peer.lat_avg, SlothDuration(42, "ms"))
        self.assertEqual(peer.lat_stddev, SlothDuration(400, "us"))
        self.assertEqual(peer.bandwidth, SlothBandwidth(21, "kbit"))

    @mock.patch("sloth.node_config.socket.gethostbyname")
    def test_failed_init(self, gethostbyname_mock: mock.Mock) -> None:
        gethostbyname_mock.side_effect = lambda hostname: FAKE_DNS[hostname]
        with self.assertRaises(ValueError):
            SlothPeer("machine01", None, "1234ms", None)

        with self.assertRaises(ValueError):
            SlothPeer("machine01", "garbage", None, None)

        with self.assertRaises(ValueError):
            SlothPeer("machine01", "1234", "garbage", None)

        with self.assertRaises(ValueError):
            SlothPeer("machine01", "1234", "1234", "garbage")

        with self.assertRaises(Exception):
            SlothPeer("garbage_machine", "1234", "1234", "1234")

    @mock.patch("sloth.node_config.socket.gethostbyname")
    def test_dict_conversion(self, gethostbyname_mock: mock.Mock) -> None:
        gethostbyname_mock.side_effect = lambda hostname: FAKE_DNS[hostname]

        d = {"hostname": "machine01"}
        peer = SlothPeer.from_dict(d)
        self.assertEqual(peer.to_dict(), d)
        self.assertEqual(peer.hostname, "machine01")
        self.assertEqual(peer.ip, "192.168.0.1")
        self.assertEqual(peer.lat_avg, None)
        self.assertEqual(peer.lat_stddev, None)
        self.assertEqual(peer.bandwidth, None)

        d = {"hostname": "machine01", "lat_avg": "42ms"}
        peer = SlothPeer.from_dict(d)
        self.assertEqual(peer.to_dict(), d)
        self.assertEqual(peer.hostname, "machine01")
        self.assertEqual(peer.ip, "192.168.0.1")
        self.assertEqual(peer.lat_avg, SlothDuration(42, "ms"))
        self.assertEqual(peer.lat_stddev, None)
        self.assertEqual(peer.bandwidth, None)

        d = {"hostname": "machine01", "lat_avg": "42ms", "bandwidth": "21kbit"}
        peer = SlothPeer.from_dict(d)
        self.assertEqual(peer.to_dict(), d)
        self.assertEqual(peer.hostname, "machine01")
        self.assertEqual(peer.ip, "192.168.0.1")
        self.assertEqual(peer.lat_avg, SlothDuration(42, "ms"))
        self.assertEqual(peer.lat_stddev, None)
        self.assertEqual(peer.bandwidth, SlothBandwidth(21, "kbit"))

        d = {"hostname": "machine01", "bandwidth": "21kbit"}
        peer = SlothPeer.from_dict(d)
        self.assertEqual(peer.to_dict(), d)
        self.assertEqual(peer.hostname, "machine01")
        self.assertEqual(peer.ip, "192.168.0.1")
        self.assertEqual(peer.lat_avg, None)
        self.assertEqual(peer.lat_stddev, None)
        self.assertEqual(peer.bandwidth, SlothBandwidth(21, "kbit"))

        d = {"hostname": "machine01", "lat_avg": "42ms", "lat_stddev": "400us", "bandwidth": "21kbit"}
        peer = SlothPeer.from_dict(d)
        self.assertEqual(peer.to_dict(), d)
        self.assertEqual(peer.hostname, "machine01")
        self.assertEqual(peer.ip, "192.168.0.1")
        self.assertEqual(peer.lat_avg, SlothDuration(42, "ms"))
        self.assertEqual(peer.lat_stddev, SlothDuration(400, "us"))
        self.assertEqual(peer.bandwidth, SlothBandwidth(21, "kbit"))

    @mock.patch("sloth.node_config.socket.gethostbyname")
    def test_eq(self, gethostbyname_mock: mock.Mock) -> None:
        gethostbyname_mock.side_effect = lambda hostname: FAKE_DNS[hostname]

        d = {"hostname": "machine01", "lat_avg": "42ms", "bandwidth": "21kbit"}
        self.assertEqual(SlothPeer.from_dict(d), SlothPeer("machine01", "42ms", None, "21kbit"))
        self.assertNotEqual(SlothPeer.from_dict(d), SlothPeer("machine01", "42ms", "21ms", "21kbit"))


class TestSlothNodeConfig(unittest.TestCase):
    @mock.patch("sloth.node_config.socket.gethostbyname")
    def test_node_config(self, gethostbyname_mock: mock.Mock) -> None:
        gethostbyname_mock.side_effect = lambda hostname: FAKE_DNS.get(hostname)
        json_dict = {
            "interface": "eth0",
            "peers": [
                {"hostname": "machine02", "lat_avg": "20ms", "lat_stddev": "5ms"},
                {"hostname": "machine03", "lat_avg": "40ms"},
                {"hostname": "machine04", "lat_avg": "60ms", "lat_stddev": "5ms", "bandwidth": "5mbit"},
                {"hostname": "machine05"},
            ],
        }
        config = SlothNodeConfig.from_dict(json_dict)
        self.assertEqual(config.to_dict(), json_dict)
        self.assertEqual(config.interface, "eth0")
        self.assertEqual(len(config.peers), 4)
        self.assertIn("machine02", config.peers)
        self.assertIn("machine03", config.peers)
        self.assertIn("machine04", config.peers)
        self.assertIn("machine05", config.peers)

    @mock.patch("sloth.node_config.socket.gethostbyname")
    def test_eq(self, gethostbyname_mock: mock.Mock) -> None:
        gethostbyname_mock.side_effect = lambda hostname: FAKE_DNS[hostname]

        config1 = SlothNodeConfig("eth0")
        config2 = SlothNodeConfig("eth0")
        config3 = SlothNodeConfig("eth0")
        config4 = SlothNodeConfig("eth0")
        config5 = SlothNodeConfig("bond0")

        peer1 = SlothPeer("machine01")
        peer2 = SlothPeer("machine02", "42ms")
        peer3 = SlothPeer("machine02", "21ms")

        config1.add_peer(peer1)
        config1.add_peer(peer2)

        config2.add_peer(peer2)
        config2.add_peer(peer1)

        config3.add_peer(peer1)

        config4.add_peer(peer1)
        config4.add_peer(peer3)

        config5.add_peer(peer1)
        config5.add_peer(peer2)

        self.assertEqual(config1, config1)
        self.assertEqual(config1, config2)
        self.assertEqual(config2, config1)

        self.assertNotEqual(config1, config3)
        self.assertNotEqual(config3, config1)

        self.assertNotEqual(config1, config4)
        self.assertNotEqual(config4, config1)

        self.assertNotEqual(config1, config5)
        self.assertNotEqual(config5, config1)
