##########################################################################################
# Copyright (c) Baltasar Dinis. All rights reserved.
# Licensed under the MIT License. See LICENSE in the project root for license information.
##########################################################################################

"""
types_test.py

types.py unit tests
"""

import unittest

from sloth.types import SlothDuration, SlothBandwidth


class TestSlothDuration(unittest.TestCase):
    def test_parse_simple(self) -> None:
        self.assertEqual(SlothDuration.parse(1234), SlothDuration(1234, "ms"))
        self.assertEqual(SlothDuration.parse("1234"), SlothDuration(1234, "ms"))
        self.assertEqual(SlothDuration.parse(SlothDuration(1234)), SlothDuration(1234, "ms"))

        self.assertEqual(SlothDuration.parse("1234ms"), SlothDuration(1234, "ms"))
        self.assertEqual(SlothDuration.parse("1234msec"), SlothDuration(1234, "ms"))
        self.assertEqual(SlothDuration.parse("1234msecs"), SlothDuration(1234, "ms"))
        self.assertEqual(SlothDuration.parse("1234us"), SlothDuration(1234, "us"))
        self.assertEqual(SlothDuration.parse("1234s"), SlothDuration(1234, "s"))

    def test_parse_spaces(self) -> None:
        self.assertEqual(SlothDuration.parse("  1234 ms  "), SlothDuration(1234, "ms"))

    def test_parse_fail(self) -> None:
        with self.assertRaises(ValueError):
            SlothDuration.parse("1234mbit")

        with self.assertRaises(ValueError):
            SlothDuration.parse("123a4ms")

        with self.assertRaises(ValueError):
            SlothDuration.parse("ms1234")

        with self.assertRaises(ValueError):
            SlothDuration.parse("ms")

        with self.assertRaises(TypeError):
            SlothDuration.parse({"value": 1234})

    def test_repr(self) -> None:
        self.assertEqual(str(SlothDuration(1234, "ms")), "1234ms")

    def test_half(self) -> None:
        self.assertEqual(SlothDuration(2, "s").half(), SlothDuration(1, "s"))
        self.assertEqual(SlothDuration(1, "s").half(), SlothDuration(500, "ms"))
        self.assertEqual(SlothDuration(3, "s").half(), SlothDuration(1500, "ms"))

        self.assertEqual(SlothDuration(2, "ms").half(), SlothDuration(1, "ms"))
        self.assertEqual(SlothDuration(1, "ms").half(), SlothDuration(500, "us"))
        self.assertEqual(SlothDuration(3, "ms").half(), SlothDuration(1500, "us"))

        self.assertEqual(SlothDuration(2, "us").half(), SlothDuration(1, "us"))
        self.assertEqual(SlothDuration(1, "us").half(), SlothDuration(0, "us"))
        self.assertEqual(SlothDuration(3, "us").half(), SlothDuration(1, "us"))


class TestSlothBandwith(unittest.TestCase):
    def test_parse_simple(self) -> None:
        self.assertEqual(SlothBandwidth.parse(1234), SlothBandwidth(1234, "bit"))
        self.assertEqual(SlothBandwidth.parse("1234"), SlothBandwidth(1234, "bit"))
        self.assertEqual(SlothBandwidth.parse(SlothBandwidth(1234)), SlothBandwidth(1234, "bit"))

        self.assertEqual(SlothBandwidth.parse("1234bit"), SlothBandwidth(1234, "bit"))
        self.assertEqual(SlothBandwidth.parse("1234kbit"), SlothBandwidth(1234, "kbit"))
        self.assertEqual(SlothBandwidth.parse("1234kibit"), SlothBandwidth(1234, "kibit"))
        self.assertEqual(SlothBandwidth.parse("1234mbit"), SlothBandwidth(1234, "mbit"))
        self.assertEqual(SlothBandwidth.parse("1234mibit"), SlothBandwidth(1234, "mibit"))
        self.assertEqual(SlothBandwidth.parse("1234gbit"), SlothBandwidth(1234, "gbit"))
        self.assertEqual(SlothBandwidth.parse("1234gibit"), SlothBandwidth(1234, "gibit"))
        self.assertEqual(SlothBandwidth.parse("1234tbit"), SlothBandwidth(1234, "tbit"))
        self.assertEqual(SlothBandwidth.parse("1234tibit"), SlothBandwidth(1234, "tibit"))

        self.assertEqual(SlothBandwidth.parse("1bps"), SlothBandwidth(8, "bit"))
        self.assertEqual(SlothBandwidth.parse("1kbps"), SlothBandwidth(8, "kbit"))
        self.assertEqual(SlothBandwidth.parse("1kibps"), SlothBandwidth(8, "kibit"))
        self.assertEqual(SlothBandwidth.parse("1mbps"), SlothBandwidth(8, "mbit"))
        self.assertEqual(SlothBandwidth.parse("1mibps"), SlothBandwidth(8, "mibit"))
        self.assertEqual(SlothBandwidth.parse("1gbps"), SlothBandwidth(8, "gbit"))
        self.assertEqual(SlothBandwidth.parse("1gibps"), SlothBandwidth(8, "gibit"))
        self.assertEqual(SlothBandwidth.parse("1tbps"), SlothBandwidth(8, "tbit"))
        self.assertEqual(SlothBandwidth.parse("1tibps"), SlothBandwidth(8, "tibit"))

    def test_parse_spaces(self) -> None:
        self.assertEqual(SlothBandwidth.parse("  1234 bit  "), SlothBandwidth(1234, "bit"))

    def test_parse_fail(self) -> None:
        with self.assertRaises(ValueError):
            SlothBandwidth.parse("1234ms")

        with self.assertRaises(ValueError):
            SlothBandwidth.parse("123a4bit")

        with self.assertRaises(ValueError):
            SlothBandwidth.parse("bit1234")

        with self.assertRaises(ValueError):
            SlothBandwidth.parse("bit")

        with self.assertRaises(TypeError):
            SlothBandwidth.parse({"value": 1234})

    def test_repr(self) -> None:
        self.assertEqual(str(SlothBandwidth(1234, "bit")), "1234bit")

    def test_half(self) -> None:
        self.assertEqual(SlothBandwidth(2, "tbit").half(), SlothBandwidth(1, "tbit"))
        self.assertEqual(SlothBandwidth(1, "tbit").half(), SlothBandwidth(500, "gbit"))
        self.assertEqual(SlothBandwidth(3, "tbit").half(), SlothBandwidth(1500, "gbit"))

        self.assertEqual(SlothBandwidth(2, "gbit").half(), SlothBandwidth(1, "gbit"))
        self.assertEqual(SlothBandwidth(1, "gbit").half(), SlothBandwidth(500, "mbit"))
        self.assertEqual(SlothBandwidth(3, "gbit").half(), SlothBandwidth(1500, "mbit"))

        self.assertEqual(SlothBandwidth(2, "mbit").half(), SlothBandwidth(1, "mbit"))
        self.assertEqual(SlothBandwidth(1, "mbit").half(), SlothBandwidth(500, "kbit"))
        self.assertEqual(SlothBandwidth(3, "mbit").half(), SlothBandwidth(1500, "kbit"))

        self.assertEqual(SlothBandwidth(2, "kbit").half(), SlothBandwidth(1, "kbit"))
        self.assertEqual(SlothBandwidth(1, "kbit").half(), SlothBandwidth(500, "bit"))
        self.assertEqual(SlothBandwidth(3, "kbit").half(), SlothBandwidth(1500, "bit"))

        self.assertEqual(SlothBandwidth(2, "bit").half(), SlothBandwidth(1, "bit"))
        self.assertEqual(SlothBandwidth(1, "bit").half(), SlothBandwidth(0, "bit"))
        self.assertEqual(SlothBandwidth(3, "bit").half(), SlothBandwidth(1, "bit"))

        self.assertEqual(SlothBandwidth(2, "tibit").half(), SlothBandwidth(1, "tibit"))
        self.assertEqual(SlothBandwidth(1, "tibit").half(), SlothBandwidth(512, "gibit"))
        self.assertEqual(SlothBandwidth(3, "tibit").half(), SlothBandwidth(1536, "gibit"))

        self.assertEqual(SlothBandwidth(2, "gibit").half(), SlothBandwidth(1, "gibit"))
        self.assertEqual(SlothBandwidth(1, "gibit").half(), SlothBandwidth(512, "mibit"))
        self.assertEqual(SlothBandwidth(3, "gibit").half(), SlothBandwidth(1536, "mibit"))

        self.assertEqual(SlothBandwidth(2, "mibit").half(), SlothBandwidth(1, "mibit"))
        self.assertEqual(SlothBandwidth(1, "mibit").half(), SlothBandwidth(512, "kibit"))
        self.assertEqual(SlothBandwidth(3, "mibit").half(), SlothBandwidth(1536, "kibit"))

        self.assertEqual(SlothBandwidth(2, "kibit").half(), SlothBandwidth(1, "kibit"))
        self.assertEqual(SlothBandwidth(1, "kibit").half(), SlothBandwidth(512, "bit"))
        self.assertEqual(SlothBandwidth(3, "kibit").half(), SlothBandwidth(1536, "bit"))

        self.assertEqual(SlothBandwidth(2, "bit").half(), SlothBandwidth(1, "bit"))
        self.assertEqual(SlothBandwidth(1, "bit").half(), SlothBandwidth(0, "bit"))
        self.assertEqual(SlothBandwidth(3, "bit").half(), SlothBandwidth(1, "bit"))
