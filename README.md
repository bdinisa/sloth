# Sloth

An easy-to-use, granular, traffic shaping tool based on Linux's tc. Written in python3.

# Setup

We recommend you use a Python virtual environment to run this tool:

```bash
virtualenv --python=python3 venv
source venv/bin/activate
python3 -m pip install --editable .
python3 -m pip install '.[dev]'  # optional: installs development dependencies
```

# Usage

## Configuration

First you need to write out a configuratoin file, in the json format. Here's an example:

```json
{
    "nodes": {
        "machine01": "eth0",
        "machine02": "wpl3s0",
        "machine03": "bond0"
    },
    "links": [
        {
            "hosts": ["machine01", "machine02"],
            "lat_avg": "20ms",
            "lat_stddev": "5ms"
        },
        {
            "hosts": ["machine02", "machine03"],
            "lat_avg": "10ms",
            "lat_stddev": "5ms",
            "bandwidth": "5Mbit"
        },
        {
            "hosts": ["machine01", "machine03"],
            "bandwidth": "10mbit"
            "directional": true
        },
        {
            "hosts": ["machine03", "machine01"],
            "bandwidth": "100mbit"
            "directional": true
        }
    ]
}
```

It describes a topology of three machines, where `machine02` has a simmetrical
link to `machine01` (with a latency of 20ms +/- 5ms) and to `machine03` (with a latency
of 10ms +/- 5ms and a bandwidth limit of 5 mbit both ways). `machine01` and
`machine03` have an asymmetrical link, where the upload from `machine01` is limited at 10 mbit
but the download from `machine03` is only limited at `100mbit`.

### Groups

Having groups of nodes collocated (or equidistant) is a common occurrence.
As such, sloths allows you to declare and use groups, by defining a list
of nodes which belong to a group and giving it an alias. There is also a special
group: `@all` which refers to all the replicas.

```json
{
    "nodes": {
        "machine01": "eth0",
        "machine02": "eth0",
        "machine03": "eth0",
        "machine04": "eth0",
        "machine05": "eth0",
    },
    "groups": {
        "@a": ["machine01", "machine02"],
        "@b": ["machine03", "machine04, "machine05"],
    },
    "links": [
        { "hosts": ["@a", "@b"], "lat_avg": "1ms" },
        { "hosts": ["@a", "machine03"], "lat_avg": "2ms" },
        { "hosts": ["machine01", "machine04"], "lat_avg": "3ms" },
    ]
}
```

Here, all nodes from group `@a` have a 1ms latency to nodes from `@b`.
However, it can be seen that the remaining links are also all between nodes from
`@a` and `@b`. Nevertheless, since they are more concrete (the link from `@a` to
`machine03` has only one group, and from `machine01` and `machine04` there are no groups).
`sloth` makes a **best effort** attempt to break ties, by using this _concreteness_ criteria.
However, note that if there is more than one link rule at the same level, this constitutes a bug in the topology configuration.

To help with this, an optional `priority` field on the link can be used to break ties.
This field is an integer. Any link with priority takes precedence over links without a priority.
The link with highest priority is chosen. If there are multiple links with highest priority, this is again a bug.

Consider the following example:
```json
{
    "nodes": {
        "machine01": "eth0",
        "machine02": "eth0",
        "machine03": "eth0",
        "machine04": "eth0",
    },
    "groups": {
        "@a": ["machine01", "machine02"],
    },
    "links": [
        {"hosts": ["@a", "@a"], "lat_avg": "2ms"},
        {"hosts": ["@a", "@all"], "lat_avg": "4ms"},
        {"hosts": ["@all", "@all"], "lat_avg": "6ms"},
    ],
}
```

All the links here are conflicting.
However, by giving them different priorities:

```json
{
    "nodes": {
        "machine01": "eth0",
        "machine02": "eth0",
        "machine03": "eth0",
        "machine04": "eth0",
    },
    "groups": {
        "@a": ["machine01", "machine02"],
    },
    "links": [
        {"hosts": ["@a", "@a"], "lat_avg": "2ms", "priority": 3},
        {"hosts": ["@a", "@all"], "lat_avg": "4ms", "priority": 2},
        {"hosts": ["@all", "@all"], "lat_avg": "6ms", "priority": 1},
    ],
}
```

This way the connections within `@a` are prioritrized, followed by connections
with someone in `@a`, and the `@all` to `@all` links being the fallback.

## CLI

We can then interact with the `sloth` CLI using multiple subcommands:

 - `apply`: apply the network emulation, as per the configuration
 - `clear`: clear the current network emulation, based on the configuration
 - `clear-iface`:clear the current network emulation, given the network interface name
 - `echo`: print the configuration (after parsing)
 - `show-iface`: show the current network emulation, given the network interface name
 - `show`: show the current network emulation, based on the configuration

# Goal

`sloth` aims to be a easier-to-use interface to Linux traffic control ([`tc`](https://man7.org/linux/man-pages/man8/tc.8.html)), focusing on providing link-level granularity.
For instance, using `sloth` one can simulate network conditions of a geo-distributed WAN inside a lab.

# Contributing Guidelines

Check [HACKING.md](HACKING.md) file for development information.
